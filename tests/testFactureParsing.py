import requests
from os import listdir
from os.path import isfile
pathToFactures="./images/"

def lecture(fichier):
    f=open(fichier,"rb")
    data=f.read()
    f.close()
    return data

#parse each factures in the {path} folder
def testFactureParsing(path):
    testFactures = listdir(path)
    compteur=0
    for facture in testFactures:
        compteur += 1
        print(f"-----------------------\nTest {compteur} : {facture}\n")
        files = {'facture':lecture(path+facture)}
        res = requests.post("http://127.0.0.1:9000/api/extract", files=files)
        print('Response from server:',res.text)
        if (res.status_code == 500):
            return "\nFailed"
    return "\nSuccess"

print(testFactureParsing(pathToFactures))