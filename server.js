const nconf = require('nconf')
const Fastify = require('fastify')
const FastifyAutoload = require('fastify-autoload')
const FastifyCors = require('fastify-cors')
const FastifyJwt = require('fastify-jwt')
const FastifySensible = require('fastify-sensible')
const { loadSettings } = require('./config/configurationAdaptor')
const { start } = require('repl')
const { resolve } = require('path')
const fileUpload = require('fastify-file-upload')

loadSettings({})

const logSeverity = nconf.get('logSeverity')

const server = Fastify({
    ignoreTrailingSlash: true,
    logger: {
        level: logSeverity
    },
})

server.register(FastifyJwt, {
    secret: nconf.get('secrets.jwt')
})

server.register(fileUpload)
server.register(FastifySensible)
server.register(FastifyCors)



server.register(FastifyAutoload, {
    // dir: resolve(__dirname, 'routes'),
    dir: resolve(__dirname, 'src/app/api/routes'),
    options: { prefix: '/api' }
})


server.listen(9000, '0.0.0.0', (err) => {
    if (err) {
        console.error(err)
        return
    }
})

process.on('SINGINT', async () => {
    console.log('stopping fastify server')
    await server.close()
    console.log('fastify server stopped')
    process.exit(0)
})

start()

module.exports = {
    server
}




