FROM node:14

WORKDIR /usr/src/app

RUN echo "deb http://deb.debian.org/debian/ stretch main" > /etc/apt/sources.list
RUN apt-get update
RUN apt-get -y --allow-unauthenticated install cmake graphicsmagick

COPY package_linux.json ./
COPY package-lock_linux.json ./

RUN mv package_linux.json package.json
RUN mv package-lock_linux.json package-lock.json

RUN npm ci --only=production

COPY . .

EXPOSE 9000
CMD [ "npm", "start" ]


