const nconf = require('nconf')
const { loadSettings } = require("./config/configurationAdaptor");

loadSettings()

const { server } = require('./server')

module.exports = { server }
