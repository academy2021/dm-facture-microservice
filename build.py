import os

os.system("docker build -t backend-server:latest ./ -f ./")
os.system("docker container rm --force BACKEND_SERVER")
os.system("docker run --name BACKEND_SERVER -d -p 9000:9000 backend-server:latest")