const nconf = require('nconf')
const _ = require('lodash')
const appSettingsPath = 'config/appSettings.json'
const loadSettings = () => {
    if(_.isEmpty(appSettingsPath)) {
        throw new Error('Configuration settings path is required.')
    }

    nconf.file({
        file: appSettingsPath,
        logicalSeparator: '.'
    })
}

module.exports.loadSettings = loadSettings;