const fs = require('fs');
const { extractTextFromImageService } = require('../../domain/noteFrais/services/extractFromNoteDeFrais');
const { convertToImage } = require("../etl/eltManager");
const { extractValuesFromObject } = require('../etl/eltManager');
const { save } = require('../../infra/dbAdapters/postgres/postgresRepository');

const extractContent = async (filePath) => {
    try {
        fs.statSync(filePath);
    } catch {
        throw new Error('Path invalid ' + filePath);
    }

    const data = fs.readFileSync(filePath);

    const contentImg = await convertToImage(data);

    const result = await extractTextFromImageService(contentImg);

    const {
        sectionEntreprise,
        sectionClient,
        sectionFacture,
        sectionDetailsTable,
        sectionTotal,
        sectionBanque
    } = Object.fromEntries(result);

    console.log('--------------Section Total------------------------------------------');
    console.log(sectionTotal);

    const extractValues = await extractValuesFromObject(Object.fromEntries(result))


    console.log('-------------------Extract Values-------------------------------------');
    console.log(extractValues);

    const dataToSave = { sectionEntreprise, sectionClient, sectionFacture, sectionDetailsTable, sectionBanque, sectionTotal, extractValues }


    console.log('--------------------DataToSave------------------------------------');
    console.log(dataToSave);
    await save(dataToSave)


    return null


}

const extractContentNoteFrais = async (filePath) => {
    try {
        fs.statSync(filePath);
    } catch {
        throw new Error('Path invalid ' + filePath);
    }

    const data = fs.readFileSync(filePath);


    const result = await extractTextFromImageService(data);


    return result

}
module.exports = { extractContent, extractContentNoteFrais }
