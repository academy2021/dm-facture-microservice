## Installation LibreOffice sur Windows :

1. Télécharger l'installeur de LibreOffice.

2. Ajouter LibreOffice dans la variable d'environnement path : 'C:\Program Files\LibreOffice\program'.


## Installation LibreOffice sur Linux :

1. Installer le "meta-package" LibreOffice avec cette commande: `sudo apt-get install libreoffice`

2. Compléter l'installation en ajoutant l'intégration au bureau: 
    Si vous utilisez Ubuntu (Gnome), utilisez cette commande: `sudo apt-get install libreoffice-gnome`

    Si vous utilisez Kubuntu (KDE), utilisez plutôt cette commande: `sudo apt-get install libreoffice-kde`

## Taper la commande : 
`npm install office-to-pdf`

## Installation gm sur Windows :

1. Télécharger et installer GraphicsMagick et ImageMagick.

2. Ajouter ImageMagick dans la variable d'environnement path : C:\ImageMagick\ImageMagick-6.8.5\bin
 
3. Ajouter GraphicsMagick dans la variable d'environnement path : C:\Program Files\GraphicsMagick-1.3.34-Q16

## Taper la commande : 
`npm install gm `

