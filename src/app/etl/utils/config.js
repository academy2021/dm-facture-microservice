const { model } = require("@tensorflow/tfjs");

const keyWords = {
    TVA: ["tva", 'total tva', 'TVA 20,00%', ' 20,00%(HT 33,25)'],
    Total: ["total", 'Total à régler'],
    Taux_TVA: ['taux Tva'],
    Sous_Total: ["sous-total"],
    Taxe: ["taxe", 'autres taxes'],
    TTC: ["ttc", 'Montant total TTC', 'Total TTC'],
    HT: ["ht", "total HT", 'Montant HT'],
    Total_Net: ["net à payer"],
    Remise: ["remise"],
    Frais_Livraison: ['Frais de livraison']
};


module.exports = { keyWords }