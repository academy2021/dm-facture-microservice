
const path = require('path');
var fs = require("fs")
var toPdf = require("office-to-pdf")

const convertOfficeToPdf = async data => {
  return toPdf(data)

}

module.exports = {
  convertOfficeToPdf
}