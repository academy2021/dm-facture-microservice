const convertPdfToImage = require('./pdfHelper')
const { convertOfficeToPdf } = require('./officeHelper')
const FileType = require('file-type')
const { keyWords } = require('./utils/config')

const convertToImage = async data => {

    const mime = await getBufferMime(data)
    switch (mime.join('/')) {
        case 'image/gif':
        case 'image/jpeg':
        case 'image/png':
            return data

        case 'application/pdf':

            return await convertPdfToImage.convert(data)

        case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
        case 'application/msword':
        case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
        case 'application/vnd.ms-excel':
        case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
        case 'application/vnd.ms-powerpoint':

            const pdf = await convertOfficeToPdf(data)
            return await convertPdfToImage.convert(pdf)


        default:
            throw new Error('NOT IMPLEMENTED')
    }
}

async function getBufferMime(data) {
    const fileType = await FileType.fromBuffer(data) // {ext: png, mime: image/png}
    if (!fileType) return
    const { mime } = fileType
    return mime.split('/') // mime examples image/png ou video/mp4
}

function extractNumbers(string) {
    const matches = string.match(/\d*[\.%,€]*\d*/g);
    return matches
        .filter(match => match)
        .join('');
}

function regexGenerator() {
    return Object.entries(keyWords).reduce((acc, [category, keyword]) => {
        const current = keyword.map(textValue => {
            const textValueLower = textValue.toLowerCase()

            return new RegExp('^' + textValueLower + "(\\s\\d*)*(,)*(.)*(\\s*\\d*)*(%)*(\\s*\\d*)*(,)*\\d*\\s*[€]*")
        });
        acc[category] = current;
        return acc;
    }, {});
}

const extractValuesFromObject = async (itemObj) => {
    const itemObjLower = itemObj.sectionTotal.toLowerCase().trim().replace('(', '').replace(')', '');
    const regexes = regexGenerator();
    const result = {};
    Object.entries(regexes).forEach(([category, lookups]) => {
        for (let lookup of lookups) {
            for (let t of itemObjLower.split('\n')) {
                const matches = lookup.exec(t);
                if (matches && matches.length) {
                    result[category] = extractNumbers(matches[0].toUpperCase());
                }
            }
        }
    })

    return result;
}





module.exports = { convertToImage, extractValuesFromObject }