const gm = require('gm')
const combineImage = require('combine-image')
const fs = require('fs')
const Jimp = require('jimp')
var options = {
    size: 2048,
    density: 600,
    quality: 100
}

var convertPdfToImage = function () { }
convertPdfToImage.prototype.setOptions = function (opts) {
    options.size = opts.size || options.size
    options.density = opts.density || options.density
    options.quality = opts.quality || options.quality
}
const getPages = (inputBuffer) => {
    return new Promise(
        (resolve, reject) => {
            gm(inputBuffer).identify('%p ', (err, value) => {
                if (err) { reject(err) }
                resolve(String(value).split(' '))
            })
        }
    )
}

convertPdfToImage.prototype.convert = async (inputBuffer) => {
    const pages = await getPages(inputBuffer)
    const buffs = await Promise.all(pages.map(
        (page) =>
            convertPdf2Img(inputBuffer, parseInt(page))
    ))
    console.log(buffs)
    const combineImgs = await combineImage(buffs)
    const jimpImg = new Jimp(combineImgs)
    return new Promise(
        (resolve, reject) => {
            jimpImg.getBuffer(Jimp.MIME_PNG, (err, data) => {
                if (err) reject(err)
                else {
                    resolve(data)
                    fs.writeFile('amani.png', data, console.log)
                }
            })

        })

}

const convertPdf2Img = (inputBuffer, page) => {
    return new Promise(
        (resolve, reject) => {
            gm(inputBuffer)
                .density(options.density, options.density)
                .resize(options.size)
                .quality(options.quality)
                .toBuffer('PNG', function (err, buffer) {
                    if (err) { reject(err) }

                    resolve(buffer)
                })
        }
    )
}

module.exports = new convertPdfToImage()



