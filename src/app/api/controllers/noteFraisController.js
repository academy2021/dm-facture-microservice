const { extractTextFromImageService } = require('../../../domain/noteFrais/services/extractFromNoteDeFrais');


const ExtractContent = async (contents) => {

    const buffer = contents.body.document.data
    console.log(contents.body);

    const result = await extractTextFromImageService(buffer);


    const data = { ...Object.fromEntries(result) }

    return data;



}



module.exports = {

    ExtractContent

}