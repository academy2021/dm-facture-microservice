const { extractTextFromImageService } = require('../../../domain/facture/services/extractTextFromImageService');
const { convertToImage } = require("../../etl/eltManager");
const { extractValuesFromObject } = require('../../etl/eltManager');
const { save, deleteFactureById } = require('../../../infra/dbAdapters/postgres/postgresRepository');





const ExtractContent = async (contents) => {
    //console.log(contents.body);
    ///!\ careful, the uploaded file must be in a "facture" category
    const buffer = contents.body.facture.data;
    const contentImg = await convertToImage(buffer);
    const result = await extractTextFromImageService(contentImg);
    const data = { ...Object.fromEntries(result) }
    return data;
}

const ExtractContentAnSave = async (contents) => {
    let fieldsToSave = [];
    try {
        fieldsToSave = JSON.parse(contents.body.fields)
        console.log(fieldsToSave)
    } catch (e) { console.log(e) }

    const buffer = contents.body.facture.data


    const contentImg = await convertToImage(buffer);

    const result = await extractTextFromImageService(contentImg);

    const extractValues = await extractValuesFromObject(Object.fromEntries(result))


    const dataTosave = { ...extractValues, ...Object.fromEntries(result) }
    if (fieldsToSave.length) {
        await save(dataTosave, fieldsToSave);
    }

    return Object.fromEntries(result);

}

const DeleteContent = async (contents) => {
    const id = contents.id;
    await deleteFactureById(id);
}

const SaveContent = async (contents) => {

    const buffer = contents.body.facture.data

    const contentImg = await convertToImage(buffer);

    const result = await extractTextFromImageService(contentImg);

    const extractValues = await extractValuesFromObject(Object.fromEntries(result))

    const dataTosave = { ...extractValues, ...Object.fromEntries(result) }
    await save(dataTosave);

    return dataTosave;

}

const TestApi = async () => {
    return 'ok'


}
module.exports = {
    TestApi,
    ExtractContent,
    SaveContent,
    ExtractContentAnSave,
    DeleteContent
}