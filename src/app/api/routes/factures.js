const { ExtractContentAnSave, ExtractContent, DeleteContent, SaveContent, TestApi } = require('../controllers/factureController')

module.exports = async function (fastify, opts) {

    fastify.post('/extract', ExtractContent)
    fastify.post('/', ExtractContentAnSave)
    fastify.delete('/:id', DeleteContent)
    fastify.post('/save', SaveContent)
    fastify.get('/test', TestApi)

}