const Labels = {
    1: { 'id': 1, 'name': 'sectionEntreprise' },
    2: { 'id': 2, 'name': 'sectionTotal' },
}


const Seuil = 0.5

const ModelPath = 'file://src/domain/noteFrais/core/model.json'

module.exports = { Seuil, Labels, ModelPath }

