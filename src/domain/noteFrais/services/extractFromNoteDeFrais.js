const openCv = require('opencv4nodejs')
const getWorker = require('tesseract.js-node')
const { Seuil, ModelPath } = require('../utils/config')
const tf = require('@tensorflow/tfjs')
require('@tensorflow/tfjs-node')
const { Labels } = require('../utils/config')


//const path = require('path')
//const fs = require('fs')


// console.log(path.join(__dirname, "../core"));


const extractTextFromImageService = async (imageBuffer) => {

    const sections = await detectionSection(imageBuffer)
    //console.log(sections.get('sectionTotal'))

    const textSections = new Map()
    let imageMatrice = openCv.imdecode(imageBuffer)
    let grayImage = imageMatrice.cvtColor(openCv.COLOR_BGR2GRAY)
    //grayImage = openCv.blur(grayImage, new openCv.Size(5,5))
    //let thresh = grayImage.threshold(127,255,openCv.THRESH_BINARY_INV)
    //openCv.imshowWait('thresh',thresh)
    //------------------
    console.log('imageMatrice extraction height ' + imageMatrice.sizes[0])
    console.log('imageMatrice extraction width ' + imageMatrice.sizes[1])
    //------------------
    const [height, width] = imageMatrice.sizes
    const worker = await getWorker({
        languages: ['fra'],
        tessdata: 'src/domain/noteFrais/core/tessdata'

    })

    for (let entry of sections.entries()) {

        const [docSection, sectionCoordinates] = entry
        const ymin = sectionCoordinates[0] * height
        const ymax = sectionCoordinates[2] * height
        const xmin = sectionCoordinates[1] * width
        const xmax = sectionCoordinates[3] * width

        const rect = new openCv.Rect(xmin, ymin, xmax - xmin, ymax - ymin)
        let region = grayImage.getRegion(rect)
        //openCv.imshowWait(docSection, region)

        region = region.resize(region.sizes[0] * 2, region.sizes[1] * 2, openCv.INTER_CUBIC)
        const regionMatrice = openCv.imencode('.jpeg', region).toString('base64')

        const text = await worker.recognize(Buffer.from(regionMatrice, 'base64'), 'fra')

        textSections.set(docSection, text)
    }

    return textSections
}

const detectionSection = async (imageBuffer) => {

    let mat = new openCv.imdecode(imageBuffer)

    //mat = mat.resize(1024,600,openCv.INTER_CUBIC)
    const matData = mat.getData()


    let [height, width] = mat.sizes
    console.log('height detection ' + height)
    console.log('width detection ' + width)
    const detections = tf.tensor3d(matData, [height, width, 3]).expandDims(0)

    const model = await tf.loadGraphModel(ModelPath)
    const detectionsResult = await model.executeAsync(detections)


    const detectionClasses = detectionsResult[2].dataSync()
    const boxesTmp = detectionsResult[0].dataSync()
    const detectionScores = detectionsResult[1].dataSync()
    const result = new Map()

    for (let i = 0; i < detectionScores.length; i++) {
        const detection_boxes = []
        if (detectionScores[i] >= Seuil) {
            //console.log(detectionScores[i])
            detection_boxes[0] = boxesTmp[i * 4]
            detection_boxes[2] = boxesTmp[(i * 4) + 2]
            detection_boxes[1] = boxesTmp[(i * 4) + 1]
            detection_boxes[3] = boxesTmp[(i * 4) + 3]

            const numClass = detectionClasses[i]
            let labelClass = Labels[numClass]['name']
            if (result.has(labelClass)) {
                //labelClass = labelClass + i
            }
            result.set(labelClass + ',' + detectionScores[i], detection_boxes)
        }
    }
    return result

}

module.exports = {
    extractTextFromImageService
}


