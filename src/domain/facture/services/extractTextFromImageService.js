const openCv = require('opencv4nodejs')
const getWorker = require('tesseract.js-node')
const { Seuil, ModelPath } = require('../utils/config')
const tf = require('@tensorflow/tfjs')
require('@tensorflow/tfjs-node')
const { Labels } = require('../utils/config')
const path = require('path')
const extractTextFromImageService = async (imageBuffer) => {


    const sections = await detectionSection(imageBuffer)
    console.log(sections)

    const textSections = new Map()
    let imageMatrice = openCv.imdecode(imageBuffer)
    const grayImage = imageMatrice.cvtColor(openCv.COLOR_BGR2GRAY)
    //------------------
    console.log('imageMatrice extraction height ' + imageMatrice.sizes[0])
    console.log('imageMatrice extraction width ' + imageMatrice.sizes[1])
    //------------------
    const [height, width] = imageMatrice.sizes
    const worker = await getWorker({
        //tessdata: '../src/domain/facture/core/tessdata',

        //tessdata: 'C:\\DocManagement\\dm-server\\src\\domain\\facture\\core\\tessdata',

        // tessdata: `${path.resolve(__dirname, '..\\core\\tessdata')}`,
        tessdata: `${path.resolve(__dirname, '..','core','tessdata')}`,
        languages: ['fra']
    })
    for (let entry of sections.entries()) {

        const [docSection, sectionCoordinates] = entry
        const [xmin,xmax,ymin,ymax] = get_coordinates(sectionCoordinates, height,width);
        const rect = new openCv.Rect(xmin, ymin, xmax - xmin, ymax - ymin)
        console.log(`(xmin,ymin,xmax-xmin,ymax-ymin) : ${xmin}, ${ymin}, ${xmax-xmin}, ${ymax-ymin}`)
        let region = grayImage.getRegion(rect)
        //openCv.imshowWait(docSection, region)
        region = region.resize(region.sizes[0] * 2, region.sizes[1] * 2, openCv.INTER_CUBIC)
        const regionMatrice = openCv.imencode('.jpeg', region).toString('base64')
        const text = await worker.recognize(Buffer.from(regionMatrice, 'base64'), 'fra')
        textSections.set(docSection, text)
    }

    return textSections
}

const get_coordinates = (sectionCoordinates, height,width) => {
    var ymin = sectionCoordinates[0] * height - 5
    var ymax = sectionCoordinates[2] * height + 5
    var xmin = sectionCoordinates[1] * width - 5
    var xmax = sectionCoordinates[3] * width + 5
    if (xmin < 0) {
        xmin=0;
    }
    if (ymin<0) {
        ymin=0;
    }
    if ((xmax-xmin) > width) {
        xmax = width - xmin;
    }
    if ((ymax-ymin) > height) {
        ymax = height - ymin;
    }
    return [xmin,xmax,ymin,ymax];
}

const detectionSection = async (imageBuffer) => {
    let mat = new openCv.imdecode(imageBuffer)
    console.log(mat.sizes)
    //mat = mat.resize(1024, 600, openCv.INTER_CUBIC)
    const matData = mat.getData()
    let [height, width] = mat.sizes
    console.log('height detection ' + height)
    console.log('width detection ' + width)
    const detections = tf.tensor3d(matData, [height, width, 3]).expandDims(0)
    const model = await tf.loadGraphModel(ModelPath)
    const detectionsResult = await model.executeAsync(detections)
    const detectionClasses = detectionsResult[0].dataSync()
    const boxesTmp = detectionsResult[2].dataSync()
    const detectionScores = detectionsResult[3].dataSync()
    const result = new Map()

    for (let i = 0; i < detectionScores.length; i++) {
        const detection_boxes = []
        if (detectionScores[i] >= Seuil) {

            detection_boxes[0] = boxesTmp[i * 4]
            detection_boxes[2] = boxesTmp[(i * 4) + 2]
            detection_boxes[1] = boxesTmp[(i * 4) + 1]
            detection_boxes[3] = boxesTmp[(i * 4) + 3]

            const numClass = detectionClasses[i]
            let labelClass = Labels[numClass]['name']
            // if (result.has(labelClass)) {
            //     labelClass = labelClass + i
            // }
            result.set(labelClass + ',' + detectionScores[i], detection_boxes)
        }
    }
    return result

}

module.exports = {
    extractTextFromImageService
}
