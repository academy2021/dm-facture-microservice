const Labels = {
    1: { 'id': 1, 'name': 'sectionEntreprise' },
    2: { 'id': 2, 'name': 'sectionClient' },
    3: { 'id': 3, 'name': 'sectionFacture' },
    4: { 'id': 4, 'name': 'sectionDetailsTable' },
    5: { 'id': 5, 'name': 'sectionTotal' },
    6: { 'id': 6, 'name': 'sectionBanque' }
}


const Seuil = 0.5
// //const ModelPath = 'file://C:\\DocManagement\\dm-server\\src\\domain\\facture\\core\\model.json'

// console.log(ModelPath);

// const ModelPath = 'file://../src/domain/facture/core/model.json'

const path = require('path')
// const ModelPath = `file://${path.resolve(__dirname, '..\\core\\model.json')}`
const ModelPath = `file://${path.resolve(__dirname, '..', 'core','model.json')}`


module.exports = { Seuil, Labels, ModelPath }

