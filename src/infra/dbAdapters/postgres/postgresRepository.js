const { db } = require('./postgresHelper')


async function save(data, fieldsToSave = []) {
    await db.init();
    let dataToSave = {};
    if (fieldsToSave.length) {
        fieldsToSave.forEach(key => {
            if (key in data) {
                dataToSave[key] = data[key];
            }
        })
    } else {
        dataToSave = data;
    }
    console.log(data, fieldsToSave)
    console.log(dataToSave)
    await db.models.Facture.create(dataToSave);
}

async function deleteFactureById(id) {
    if (!id) return;
    await db.init();
    return await db.models.Facture.destroy({
        where: {
            id //this will be your id that you want to delete
        }
    })
}




module.exports = { save, deleteFactureById }

