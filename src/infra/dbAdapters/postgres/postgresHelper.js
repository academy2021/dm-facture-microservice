
const Sequelize = require('sequelize');
const path = require('path');
const { Facture } = require('../models/facture')
const { sequelize } = require('../postgres/connexion');
const Umzug = require('umzug');


// CREATE DATABASE "document_management";
// CREATE EXTENSION "uuid-ossp";


class DB {
    constructor() {
        this.initialized = false;
        this.sequelize = sequelize;
        this.models = { Facture };
    }

    async init() {
        if (this.initialized) return;
        await this.migrate();
        this.initialized = true;

    }

    async migrate() {
        const umzug = new Umzug({
            storage: 'sequelize',
            storageOptions: {
                sequelize
            },
            migrations: {
                params: [
                    sequelize.getQueryInterface(),
                    Sequelize
                ],
                path: path.join(__dirname, './migrations'),
                pattern: /\.js$/
            }
        });
        const migrations = await umzug.up();
        console.info('db migrations: ', migrations);
    }

}

const db = new DB();

module.exports = { db };