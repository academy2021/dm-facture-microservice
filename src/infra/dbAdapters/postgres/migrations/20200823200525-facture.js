'use strict';

const fs = require('fs');
const path = require('path');

module.exports = {
  up(queryInterface) {
    return new Promise((resolve, reject) => {
      const filePath = path.join(__dirname, '/20200823200525-facture.sql');
      fs.readFile(filePath, 'utf8', (error, sql) => {
        if (error) {
          reject(error);
          return;
        }

        queryInterface.sequelize
          .query(sql, { raw: true })
          .then(resolve);
      });
    });
  },

  down(queryInterface) {
    return queryInterface.dropAllTables();
  }
};
