const { TEXT } = require('sequelize');
const { sequelize } = require('../postgres/connexion');
const Sequelize = require('sequelize');

const Facture = sequelize.define('Facture', {

    sectionEntreprise: Sequelize.TEXT(),
    sectionClient: Sequelize.TEXT(),
    sectionFacture: Sequelize.TEXT(),
    sectionDetailsTable: Sequelize.TEXT(),
    sectionBanque: Sequelize.TEXT(),
    sectionTotal: Sequelize.TEXT(),
    TVA: Sequelize.TEXT(),
    Taux_TVA: Sequelize.TEXT(),
    Taxe: Sequelize.TEXT(),
    Total: Sequelize.TEXT(),
    Sous_Total: Sequelize.TEXT(),
    Remise: Sequelize.TEXT(),
    HT: Sequelize.TEXT(),
    TTC: Sequelize.TEXT(),
    Total_Net: Sequelize.TEXT(),
    Frais_Livraison: Sequelize.TEXT(),

});


module.exports = {
    Facture

}